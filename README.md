# Mass-decorrelated-Xbb-tagger
Tools for training and study of mass de-correlated Xbb tagger using deep neural network
by Wei Ding wei.ding@cern.ch .
This is a fork of his [original repo](https://gitlab.cern.ch/ding/massdecorrelatedxbbtagger).

1. reweight
1. process
1. prepare
1. train
1. study


The instructions for how to run are in the file run.sh in each directory.
This tool is inherited from the [tool by Julian Collado Umana](https://gitlab.cern.ch/atlas-boosted-hbb/xbb-tagger-training).
Here is the input MC samples:[https://gitlab.cern.ch/atlas-boosted-xbb/xbb-datasets](https://gitlab.cern.ch/atlas-boosted-xbb/xbb-datasets).
More studies of this tagger are found [https://indico.cern.ch/event/864911/](https://indico.cern.ch/event/864911/). 

A validation pacakge, [https://gitlab.cern.ch/atlas-boosted-xbb/xbb-validation/](https://gitlab.cern.ch/atlas-boosted-xbb/xbb-validation/) is not at all integrated to this package but has some very useful tools!


# Download files
rucio download the datasets listed here [p3990 v3](https://gitlab.cern.ch/atlas-boosted-xbb/xbb-datasets/-/blob/master/p3990/mc16d-h5-v3-datasets.txt) and original dataset names are [here](https://gitlab.cern.ch/atlas-boosted-xbb/xbb-datasets/-/blob/master/p3990/mc16d-input-datasets.txt)

## Jets
To quickly R&D JZ3W (361023) to JZ8W (361028) are enough.
`mkdir DijetsDatasets` and put the datasets in there (or softlink)

## Higgs
To quickly R&D use the SM boosted Higgs sample 309450.
`mkdir HbbDatasets` and put the datasets in there (or softlink)

# Run Reweight
Go to the directory, from the base dir, `cd reweight`.

Some helper scripts exist 
1. `printOriginalH5.py`: opens and dumps some things from the original h5. Useful for exploring what is available
1. `printINFO.py`: opens and dumps the output files (labeled files).

## Dijet Weights
First run `calculatedDijetsWeights.py` to get the sample weights needed to combine the dijet samples into a smooth distributions accounting for cross-section and filter efficiencies.
```
python calculatedDijetsWeights.py --path path/to/DijetsDatasets/ (include DijetDatasets in the path)
```
Part of the output to the screen is needed in the next script `labelDijetsDatasets.py`. It is the set of numbers in curly brackets `{}` and should be assigned to `listSection`.

## Run Label
The purpose of this step is to add a few columns to the initial h5 files including the total event weight and a label of the process.

There are three, and each is designed to take the same path. Therefore the path should not include the directory with a name reflecting the sample content. For example if the dijet datasets are in `/eos/user/s/student/samples/DijetDatasets/` then the path to use for all these scripts is `/eos/user/s/student/samples/`.

These scripts pull as subset of information from the original h5. It currently using DL1r (not DLrT) and saves quite a few JSS variables.

**To check - seg fault when saving eventNumber?**

_NOTE: At the time of writing this, it is not immediately clear why this step is needed before the process step._

### Jets
```
python labelDijetsDatasets.py --path path/to/samples
```
The output goes to `../../ReducedDijets/`. The output is quite big, so it would be good to softlink and eos diectory to the output directory or to change the output directory to point to your eos directory.

This is time consuming so best let it run for a while. To run on one DISD use `--dsid 3610XX`. Parallelize by running 1 DSID per shell might be needed. Also, to speed things up, a limited number of h5 files can be used from each dataset, but that has to be consistently done from the start.

### Hbb
```
python labelHbbDatasets.py --path path/to/samples
```
It expects there to be a directory inside path/to/samples with the name `HbbDatasets`
The output goes to `../../ReducedHbb/`. The output is quite big, so it would be good to softlink and eos diectory to the output directory or to change the output directory to point to your eos directory.

** Does the original h5 only consider the leading jet in the event? Seems like 1/2 of the Hbb events are lost which is compatible with only looking at the leading jet **

__labelHbbDatasets is only a few lines different from labelDijetDatasets__

## Run Merge
Now the files for all the DSIDs of Hbb files into one
```
python MergeDatasets.py --path ../../ReducedHbb  --outname MergedHbb.h5
```
This merging step is done from the process directory for dijets.

__the workflow of the various samples should be in the same directories___


# Run Process
Go to the directory, from the base dir, `cd process`.
The purpose of these scripts are to take a reduced set of information from the output of the previous step and write it as an array in a file instead of a dataframe. This forces the next steps to rely on picking the correct position in the array to get the value needed. The script was altered to perform this step at the very end new_hdf5.create_dataset("data",data=df.values) instead of the beginning to improve its robustness when changing input variables.
The DL1r variable is calculated as `log(pb/((1-f)*pc+f*pl))`. A binned version is also computed.

**It seems as though the current reweight and process steps can be easily combined.**


## Merge Dijets
Go to the directory `cd mergeDijets`.

First thing to do is flatten the samples using `flattenDijets.py`. A flag for the output dir is now available, following the original structure of dirs. The output dir structure will mimic the struture inside ReducedDijets (it assumes there is a set of directories and within each are the h5s). It currently runs from this dir with:
```
for i in $(ls ../../../ReducedDijets/*/*h5)
do
  python flattenDijets.py --path ${i} --outdir ../../../FlattenData/AllDijets/
done
```
This script takes some time to run, and has large output, so again make and softlink the ouput dir to an eos directory before running.

After the flattendDijets is run, next is `MergeDijetsDSID.py` to make one h5 file per DSID. It can run as
```
path="../../../FlattenData/AllDijets/"
for i in 361023 361024 361025 361026 361027 361028 361029
do
  python MergeDijetsDSID.py --path $path --dsid $i
done
```
The output directory is hardcoded `../../../FlattenData/MergedDijetsDSID/`.

Next, take all the files which contain a complete DSID and merge them into one big dijet file. Run from this dir as 
```
python MergeDijets.py --path ../../../FlattenData/MergedDijetsDSID/
```
The output directory is hardcoded `../../../FlattenData/`. 

__`subsampleDijets.py` does not seem to be needed. The last two steps can be combined into one__

## Flatten everything else
Go back to `process`. In here, the `flatten.py` script is almost the exactly the same as `flattenDijets.py` - one expects a dir of dirs filled with h5 the other expects a dir with h5 directly. These scripts can be compressed into one (if this step is even maintained).

For the Higgs samples, run
```
python flatten.py --path ../../MergedData/MergedHbb.h5 --outdir ../../FlattenData/
```
This script takes some time to run.

## Split
The `split.py` file will split each flattened, merged dataset into a training, testing, and validation sample.
Some hardcoded values for how many jets are put into each sample was replaced with a 70/15/15% split (as a rough starting point).
```
for i in $(ls ../../FlattenData/*.h5)
do
    path=$i
    python split.py  --path $path
done
```
The output directory is hardcoded as `../../SplitData/`. Make it before running.

# Run Prepare
Go to the directory, from the base dir, `cd prepare`.
This step needs to be run on a computer where keras is available. 

## Prepare
It takes the merged/flattened samples from the previous steps and makes a single file `train.h5` with three datasets (`train`, `valid`, `test`). Three columns are added to categorize the samples for training. 

The events are reweighted but it was not clear to exactly what. The code was commented for dijets where the sum of weights was normalized to 2M (hardcoded). Changed now to move the average weight to 1 for the smallest sample and all other samples are weighted to match the integral of this sample (Higgs for the moment)
```
python prepare.py --path ../../SplitData/
```
The output directory is hardcoded as `../../PrepareData`

## Calculate Mean and Std. Dev.
Calculates the mean and standar deviation of each variable for the training. Uses magic numbers which is not great...
```
python calculateMean.py --path ../../PrepareData/train.h5
```
Output is hardcoded to `../../PrepareData/meanstd.h5`.

## Scale by Mean and Std. Dev.
Applies the scaling derived on the training dataset to the training, testing, and validation datasets.
```
python scaling.py --path ../../PrepareData/train.h5
```
Output is hardcoded to `../../PrepareData/trainstd.h5`
__Curious if this step is required, since the training has a bathc normalization layer. However, the one line `np.nan_to_num` is probably critical__

# Train
Go to the directory, from the base dir, `cd train`.
Two training scripts in the package left as-is for reference. `train.py` is a working copy for this fork
```
python train.py
```
Input is hardcoded as  `../../PrepareData/trainstd.h5`





