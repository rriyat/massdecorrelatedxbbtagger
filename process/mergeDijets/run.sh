for i in $(ls ../../../ReducedDijets/*/*h5)
do
	path=$i
	echo $j,$path
	python flattenDijets.py --path $path
	let j=j+1
done


#Merge Dijets samples by DSID
path="../../../FlattenData/AllDijets/"
for i in 361023 361024 361025 361026 361027 361028 361029
do
	python MergeDijetsDSID.py --path $path --dsid $i
done


# Subsample does not seem to be used downstream?

##Dijets subsample
#for i in $(ls ../../DataVRGhost/FlattenData3a/MergedDijetsDSID/)
#do
#	path="../../DataVRGhost/FlattenData3a/MergedDijetsDSID/"$i
#	python subsampleDijets.py --path $path
#done


#Merge Dijets samples
python MergeDijets.py --path "../../../FlattenData/MergedDijetsDSID/"


