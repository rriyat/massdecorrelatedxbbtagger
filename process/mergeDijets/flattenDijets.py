import pandas as pd
import glob,h5py
import numpy as np
import argparse,math,os
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--path", dest='path',  default="", help="path")
parser.add_argument("--outdir", dest='outdir',  default="../../../FlattenData/AllDijets/", help="outdir")
parser.add_argument("--check", dest='check',  default=False, help="Check if features are in dataset, then exit")
args = parser.parse_args()

# need to add eventNumber back
#features_group1=['weight','relativeDeltaRToVRJet_1','relativeDeltaRToVRJet_2','relativeDeltaRToVRJet_3','eventNumber','dsid','mass','pt','eta'] #0-9
features_group1=['weight','relativeDeltaRToVRJet_1','relativeDeltaRToVRJet_2','relativeDeltaRToVRJet_3','dsid','mass','pt','eta'] #0-9
#features_group2=['DL1rT_pu_1', 'DL1rT_pc_1', 'DL1rT_pb_1','DL1rT_pu_2', 'DL1rT_pc_2', 'DL1rT_pb_2','DL1rT_pu_3', 'DL1rT_pc_3', 'DL1rT_pb_3']
features_group2=['DL1r_pu_1', 'DL1r_pc_1', 'DL1r_pb_1','DL1r_pu_2', 'DL1r_pc_2', 'DL1r_pb_2','DL1r_pu_3', 'DL1r_pc_3', 'DL1r_pb_3'] #9-27
#features_group3=['DL1r_pu_1', 'DL1r_pc_1', 'DL1r_pb_1','DL1r_pu_2', 'DL1r_pc_2', 'DL1r_pb_2','DL1r_pu_3', 'DL1r_pc_3', 'DL1r_pb_3'] #9-27
features_group4=['Split12', 'Split23', 'Qw', 'PlanarFlow', 'Angularity', 'Aplanarity', 'ZCut12', 'KtDR', 'C2', 'D2', 'e3', 'Tau21_wta', 'Tau32_wta', 'FoxWolfram20'] #27-41
features_group5=['JetFitter_N2Tpair_1', 'JetFitter_dRFlightDir_1', 'JetFitter_deltaeta_1', 'JetFitter_deltaphi_1', 'JetFitter_energyFraction_1', 'JetFitter_mass_1', 'JetFitter_massUncorr_1', 'JetFitter_nSingleTracks_1', 'JetFitter_nTracksAtVtx_1', 'JetFitter_nVTX_1', 'JetFitter_significance3d_1','SV1_L3d_1', 'SV1_Lxy_1', 'SV1_N2Tpair_1', 'SV1_NGTinSvx_1', 'SV1_deltaR_1', 'SV1_dstToMatLay_1', 'SV1_efracsvx_1', 'SV1_masssvx_1', 'SV1_significance3d_1', 'rnnip_pu_1', 'rnnip_pc_1', 'rnnip_pb_1', 'rnnip_ptau_1', 'JetFitter_N2Tpair_2', 'JetFitter_dRFlightDir_2', 'JetFitter_deltaeta_2', 'JetFitter_deltaphi_2', 'JetFitter_energyFraction_2', 'JetFitter_mass_2', 'JetFitter_massUncorr_2', 'JetFitter_nSingleTracks_2', 'JetFitter_nTracksAtVtx_2', 'JetFitter_nVTX_2', 'JetFitter_significance3d_2', 'SV1_L3d_2', 'SV1_Lxy_2', 'SV1_N2Tpair_2', 'SV1_NGTinSvx_2', 'SV1_deltaR_2', 'SV1_dstToMatLay_2', 'SV1_efracsvx_2', 'SV1_masssvx_2', 'SV1_significance3d_2',  'rnnip_pu_2', 'rnnip_pc_2', 'rnnip_pb_2', 'rnnip_ptau_2', 'JetFitter_N2Tpair_3', 'JetFitter_dRFlightDir_3', 'JetFitter_deltaeta_3', 'JetFitter_deltaphi_3', 'JetFitter_energyFraction_3', 'JetFitter_mass_3', 'JetFitter_massUncorr_3', 'JetFitter_nSingleTracks_3', 'JetFitter_nTracksAtVtx_3', 'JetFitter_nVTX_3', 'JetFitter_significance3d_3', 'SV1_L3d_3', 'SV1_Lxy_3', 'SV1_N2Tpair_3', 'SV1_NGTinSvx_3', 'SV1_deltaR_3', 'SV1_dstToMatLay_3', 'SV1_efracsvx_3', 'SV1_masssvx_3', 'SV1_significance3d_3', 'rnnip_pu_3', 'rnnip_pc_3', 'rnnip_pb_3', 'rnnip_ptau_3'] #41-113

### to check if features exist

if args.check:
    print("Checking features")
    df = pd.read_hdf(args.path)
    cols = df.columns
    for c in cols:
        print(c)
    print("Checked features, exiting")
    print("Set to false to continue")
    exit()


# expect path to be an h5 file in a directory for the given dsid
# put new file (with same name) into new directory (with same name) -- only change base directory (outdir)
new_file_name=args.path.split("/")[-1] # file name
new_dir_name=args.path.split("/")[-2]  # dsid directory
basedir=args.outdir
# make all the directories if needed
basedir+="/"+new_dir_name+"/"
#print(new_file_name)
#print(new_dir_name)
#print(basedir)
if not os.path.isdir(basedir): 
    os.makedirs(basedir)
    #os.makedirs(basedir, exist_ok=True) # Python 3.5 

features=[features_group1+features_group2+features_group4]
print("Features")
print(features)

new_hdf5 = h5py.File(basedir+new_file_name, 'w')
#df = pd.read_hdf(args.path, start=0, stop=10, columns=features) # testing
df = pd.read_hdf(args.path, columns=features)
#df['dsid']=pd.to_numeric(df['dsid'])

nRowOrig = len(df.index)
print("Rows: " + str(nRowOrig))

# COULD MAKE INTO CLEAN FUNCTIONS

# cut all events with pT>cut
ptCut=1000
print("Cutting on jet pT < " + str(ptCut))
#print("First 5 jet pTs are:")
#print(df.iloc[0:5]['pt'])
df.drop(df[df['pt'] > ptCut].index, inplace=True)
print("Remaining : " + str(float(len(df.index)/float(nRowOrig))))

# cut on relativeDeltaRToVRJet - require at least 1 with relative dR>1
relDrMin=1.0
print("Cutting on relativeDeltaRToVRJet < " + str(relDrMin))
#print("First 5 jet relDRs are:")
#print(df.iloc[0:5]['relativeDeltaRToVRJet_1'])
df.drop(df[(df['relativeDeltaRToVRJet_1'] < relDrMin) & (df['relativeDeltaRToVRJet_2'] < relDrMin) & (df['relativeDeltaRToVRJet_3'] < relDrMin) ].index, inplace=True)
print("Remaining : " + str(float(len(df.index)/float(nRowOrig))))


print("Flag overlapping VR jets")
# if relativeDeltaRToVRJet is < 1, flavor labeling is unreliable so set to nan
df[['DL1r_pu_1', 'DL1r_pc_1', 'DL1r_pb_1']] = df[['DL1r_pu_1', 'DL1r_pc_1', 'DL1r_pb_1']].where(df['relativeDeltaRToVRJet_1'] > relDrMin, other=np.nan)
df[['DL1r_pu_2', 'DL1r_pc_2', 'DL1r_pb_2']] = df[['DL1r_pu_2', 'DL1r_pc_2', 'DL1r_pb_2']].where(df['relativeDeltaRToVRJet_2'] > relDrMin, other=np.nan)
df[['DL1r_pu_3', 'DL1r_pc_3', 'DL1r_pb_3']] = df[['DL1r_pu_3', 'DL1r_pc_3', 'DL1r_pb_3']].where(df['relativeDeltaRToVRJet_3'] > relDrMin, other=np.nan)


print("Calculate DL1r variables")
#Add baseline DL1r values to a new column for each subjet
f=0.018
subjets = ['1', '2', '3']
# loop over indices and build variable
for sj in subjets:
  print("\t subject " + sj)
  df['DL1r_'+sj] = df.apply(lambda row: row['DL1r_pb_'+sj] / ( (1-f)*row['DL1r_pu_'+sj] + f*row['DL1r_pc_'+sj] ), axis=1)

  #Replace nan with small number
  df[['DL1r_'+sj]] = df[['DL1r_'+sj]].where( ~np.isnan(df['DL1r_'+sj]), other=1e-15)

  #Replace inf with small number
  df[['DL1r_'+sj]] = df[['DL1r_'+sj]].where( ~np.isinf(df['DL1r_'+sj]), other=1e-15)

  #Log and clip ... following the original 
  df[['DL1r_'+sj]] = np.log(np.clip( df[['DL1r_'+sj]], 1e-30, 1e30))



  #Add a binned DL1r - copy code from above - terrible
  df['DL1rBin_'+sj] = df.apply(lambda row: row['DL1r_pb_'+sj] / ( (1-f)*row['DL1r_pu_'+sj] + f*row['DL1r_pc_'+sj] ), axis=1)

  #Replace nan with nan...? Following exactly Wei's logic but seems redundant
  df[['DL1rBin_'+sj]] = df[['DL1rBin_'+sj]].where( ~np.isnan(df['DL1rBin_'+sj]), other=np.nan)

  #Replace inf with nan
  df[['DL1rBin_'+sj]] = df[['DL1rBin_'+sj]].where( ~np.isinf(df['DL1rBin_'+sj]), other=np.nan)

  #Is there a more compact way of doing thins?
  df[['DL1rBin_'+sj]] = df[['DL1rBin_'+sj]].where( df['DL1rBin_'+sj] <  4.805, other=5 )
  df[['DL1rBin_'+sj]] = df[['DL1rBin_'+sj]].where( df['DL1rBin_'+sj] <  3.515, other=4 )
  df[['DL1rBin_'+sj]] = df[['DL1rBin_'+sj]].where( df['DL1rBin_'+sj] <  2.585, other=3 )
  df[['DL1rBin_'+sj]] = df[['DL1rBin_'+sj]].where( df['DL1rBin_'+sj] <  1.085, other=2 )
  df[['DL1rBin_'+sj]] = df[['DL1rBin_'+sj]].where( df['DL1rBin_'+sj] >= 1.085, other=1 )



print("write")
new_hdf5.create_dataset("data",data=df.values)




