##Flatten samples
#for i in $(ls ../../MergedData/*.h5)
#do
#	path=$i
#	python flatten.py  --path $path --outdir ../../FlattenData/
#done

#Split samples into training validation and testing
for i in $(ls ../../FlattenData/*.h5)
do      
        path=$i
        python split.py  --path $path
done
