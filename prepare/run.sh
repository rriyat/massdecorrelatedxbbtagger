#Prepare training and testing samples
python prepare.py --path ../../SplitData

##Calculate mean and std
python calculateMean.py --path ../../PrepareData/train.h5

##Scaling samples
python scaling.py --path ../../PrepareData/train.h5

