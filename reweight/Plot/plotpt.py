import pandas as pd
import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import argparse
import glob

parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--path", dest='path',  default="", help="path")
parser.add_argument("--outname", dest='outname',  default="", help="outname")
args = parser.parse_args()
#Parse path to samples
pathDijets = glob.glob(args.path+"/ReducedDijets/*.h5/*.h5")
pathHbb = glob.glob(args.path+"/ReducedHbb/*.h5")
ptDijets=np.array([])
dijetsWeights=np.array([])
ptHbb=np.array([])
HbbWeights=np.array([])

#Hbb samples
for i in pathHbb:
    #Read each h5 file
    print("Reading: " + i.split("/")[-1])
    hdf=pd.read_hdf(i)
    #Extract pts
    ptSample=(np.float64(hdf[['pt']]))
    weight=(np.float64(hdf[['weight']]))
    ptHbb=np.append(ptHbb, ptSample)
    HbbWeights=np.append(HbbWeights, weight)
        
##Dijet samples
for f in pathDijets:
    #Read each h5 file
    print("Reading: " + f.split("/")[-2])
    hdf=pd.read_hdf(f)
    #Extract pts
    ptSample=(np.float64(hdf[['pt']]))
    #Extract weights
    weight=(np.float64(hdf[['weight']]))
    #Concatenate Samples
    ptDijets=np.append(ptDijets, ptSample)
    #Concatenate Weights
    dijetsWeights=np.append(dijetsWeights, weight)

#Plot the samples
plt.hist(ptDijets, log=True, bins=100, histtype='step', label='Dijets', normed=True, weights=dijetsWeights, stacked=True)
plt.hist(ptHbb, log=True, bins=100, histtype='step', label='Hbb', normed=True, weights=HbbWeights, stacked=True)
plt.legend(loc='upper right')
plt.xlabel('pt [GeV]')
plt.xlim([200,1000])
plt.autoscale(enable=True, axis='y', tight=True)
plt.ylabel('Events')
plt.savefig('ptDistribution.png')
print("Saved to ptDistribution.png")
