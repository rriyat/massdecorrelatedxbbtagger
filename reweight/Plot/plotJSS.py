import pandas as pd
import numpy as np
import matplotlib as mpl
mpl.use('Agg')
from matplotlib.backends.backend_pdf import PdfPages
pp = PdfPages('plotLinear.pdf')
import matplotlib.pyplot as plt
from scipy.stats import chisquare
import argparse
import glob

parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--path", dest='path',  default="", help="path")
parser.add_argument("--outname", dest='outname',  default="", help="outname")
args = parser.parse_args()
#Parse path to samples
pathDijets = glob.glob(args.path+"/ReducedDijets/*.h5/*.h5")
pathHbb = glob.glob(args.path+"/ReducedHbb/*.h5")

HbbDf=pd.DataFrame()
for i in pathHbb:
    print("Reading: " + i.split("/")[-1])
    hdf=pd.read_hdf(i)
    hdf=hdf.dropna()
    hdf=hdf[hdf.mass < 200]
    hdf=hdf[hdf.mass > 50]
    hdf=hdf[hdf.pt < 1000]
    HbbDf=pd.concat([HbbDf, hdf], axis=0)

dijetsDf=pd.DataFrame()
for j in pathDijets:
    print("Reading: " + j.split("/")[-2])
    hdf=pd.read_hdf(j)
    hdf=hdf.dropna()
    hdf=hdf[hdf.mass < 200]
    hdf=hdf[hdf.mass > 50]
    hdf=hdf[hdf.pt < 1000]
    dijetsDf=pd.concat([dijetsDf, hdf], axis=0)

#List of variables to plot
list1=['Split12', 'Split23', 'Qw', 'PlanarFlow', 'Angularity', 'Aplanarity', 'ZCut12', 'KtDR', 'pt', 'eta', 'mass', 'e3', 'C2', 'D2', 'Tau21_wta', 'Tau32_wta', 'FoxWolfram20']

#Higgs Weights
wH=np.float64(HbbDf['weight'])
#Dijets Weights
wD=np.float64(dijetsDf['weight'])

count = 0
for p in list1:
    x1=np.float64(HbbDf[p])
    x2=np.float64(dijetsDf[p]) 
    plt.figure(count)
    nH, Hbins, Hpatches =plt.hist(x1, label='Hbb', weights=wH, log=False, bins=100, histtype='step', stacked=True, normed=True, color='red')
    nD, Dbins, Dpatches =plt.hist(x2, label='Dijets', weights=wD, log=False, bins=Hbins, histtype='step', stacked=True, normed=True, color='black')
    plt.legend(loc='upper right')
    plt.xlabel(p)
    plt.ylabel('Events')
    plt.savefig(pp, format='pdf')
    #print("Saved " + str(p) + " to plotLinear.pdf")
    
    #Calculate delta degrees of freedom
    delta=((len(nH)-np.count_nonzero(nH))-(len(nD)-np.count_nonzero(nD)))
    print("Chi2 values for "+ str(p)+ " distributions")
    print(chisquare(nH,nD,ddof=delta))
    count += 1
pp.close()
