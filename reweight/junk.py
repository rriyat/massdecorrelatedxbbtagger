import numpy as np

edges = np.concatenate([[-np.inf], np.linspace(250, 3e3, 51), [np.inf]]) 
centers = (edges[1:] + edges[:-1]) / 2
print(edges)
print("\n")
print(centers)
